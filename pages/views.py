from django.shortcuts import render
from realtors.models import Realtor
# Create your views here.
def index(request):
	# realtors=Realtor.objects.all()
	popularListings=Listings.objects.all()[:8]
	latestListings=Listings.objects.all().order_by('-id')[:8]
	context={
		"popularListings":popularListings,
		"latestListings":latestListings,
		'title':'Ghar Aagan',
	# 'realtors':realtors,
	}
	return render(request, 'pages/index.html', context)

def search(request):
	keyword=request.POST['keyword']
	realtors=Realtor.objects.filter(name__contains=keyword)
	context={
	'keyword':keyword,
	'realtors':realtors
	}
	return render(request,'pages/search.html',context)