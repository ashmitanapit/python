from django.shortcuts import render
from realtors.models import Realtor

# Create your views here.
def listRealtors(request):
	 realtorsList=Realtor.objects.all()
	 context={
	    'realtors':realtorsList
	 }
	 return render(request,'realtors/realtors.html', context)


def viewRealtor(request,realtorid):
 	realtor=Realtor.objects.get(id=realtorid)
 	context={
 		'realtor':realtor
 	}
 	return render(request,'realtors/realtor.html', context)
